//Author: Ruben Deckers
//Date: 02-07-2024

//Include statements
#include <stdio.h>


//Function declaration
void testFunction(int myNum);

//Main
int main() {
    testFunction(24);
    testFunction(-10);
    testFunction(139);
    testFunction(1024);
    return 0;
}

//Function definition
void testFunction(int myNum) {
    printf("This function prints the parameter myNum: %d \n", myNum);
}