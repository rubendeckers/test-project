//Include statements and namespace
#include <iostream>
#include <string>
using namespace std;

//Function declaration
void testFunction(int myNum=12);

//Main
int main() {
    testFunction(37);
    testFunction(-12);
    testFunction(8999);
    return 0;
}

//Function definition
void testFunction(int myNum) {
    cout << "This function prints the parameter myNum: " << myNum << "\n";
}
